# -*- coding: cp1252
import sys, os, config

def restart_program():
 """ Function that restarts the application if is executed."""
 config.main.write()
 python = sys.executable
 os.execl(python, python, * sys.argv)