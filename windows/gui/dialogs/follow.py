# -*- coding: utf-8 -*-
import wx
import twitter
from twython import TwythonError
import output

class follow(wx.Dialog):
 def __init__(self, parent, default):
  self.parent = parent
  wx.Dialog.__init__(self, None, -1)
  panel = wx.Panel(self)
  userSizer = wx.BoxSizer()
  self.SetTitle(u"Action")
  if self.parent.name_buffer == "followers" or self.parent.name_buffer == "friends":
   list = [self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()]["screen_name"]]
  else:
   list =twitter.utils.get_all_users(self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()], self.parent.db)
  self.cb = wx.ComboBox(panel, -1, choices=list, value=list[0])
  self.cb.SetFocus()
  userSizer.Add(self.cb)
  actionSizer = wx.BoxSizer(wx.VERTICAL)
  label2 = wx.StaticText(panel, -1, u"Action")
  self.follow = wx.RadioButton(panel, -1, u"Follow", style=wx.RB_GROUP)
  self.unfollow = wx.RadioButton(panel, -1, u"Unfollow")
  self.reportSpam = wx.RadioButton(panel, -1, u"Report as spam")
  self.block = wx.RadioButton(panel, -1, u"Block")
  self.setup_default(default)
  actionSizer.Add(label2)
  actionSizer.Add(self.follow)
  actionSizer.Add(self.unfollow)
  actionSizer.Add(self.reportSpam)
  actionSizer.Add(self.block)
  sizer = wx.BoxSizer(wx.VERTICAL)
  ok = wx.Button(panel, wx.ID_OK, u"OK")
  ok.Bind(wx.EVT_BUTTON, self.onok)
  cancel = wx.Button(panel, wx.ID_CANCEL, u"Cancel")
  btnsizer = wx.BoxSizer()
  btnsizer.Add(ok)
  btnsizer.Add(cancel)
  sizer.Add(userSizer)
  sizer.Add(actionSizer)
  sizer.Add(btnsizer)
  panel.SetSizer(sizer)
  self.Bind(wx.EVT_CHAR_HOOK, self.onEscape, self.cb)

 def onEscape(self, ev):
  if ev.GetKeyCode() == wx.WXK_RETURN:
   self.onok(wx.EVT_BUTTON)
  ev.Skip()

 def onok(self, ev):
  if self.follow.GetValue() == True:
   try:
    self.parent.twitter.twitter.create_friendship(screen_name=self.cb.GetValue())
    output.speak(u"Now you follow %s" % (self.cb.GetValue()), True)
    self.Destroy()
   except TwythonError as err:
    output.speak("Error %s: %s" % (err.error_code, err.msg), True)
  elif self.unfollow.GetValue() == True:
   try:
    self.parent.twitter.twitter.destroy_friendship(screen_name=self.cb.GetValue())
    output.speak("You stopped following %s" % (self.cb.GetValue()), True)
    self.Destroy()
   except TwythonError as err:
    output.speak("Error %s: %s" % (err.error_code, err.msg), True)
  elif self.reportSpam.GetValue() == True:
   try:
    self.parent.twitter.twitter.report_spam(screen_name=self.cb.GetValue())
    output.speak("%s has been reported as spam." % (self.cb.GetValue()), True)
    self.Destroy()
   except TwythonError as err:
    output.speak("Error %s: %s" % (err.error_code, err.msg), True)
  elif self.block.GetValue() == True:
   try:
    self.parent.twitter.twitter.create_block(screen_name=self.cb.GetValue())
    output.speak("%s has been blocked." % (self.cb.GetValue()), True)
    self.Destroy()
   except TwythonError as err:
    output.speak("Error %s: %s" % (err.error_code, err.msg), True)

 def setup_default(self, default):
  if default == "follow":
   self.follow.SetValue(True)
  elif default == "unfollow":
   self.unfollow.SetValue(True)
  elif default == "report":
   self.reportSpam.SetValue(True)
  elif default == "block":
   self.block.SetValue(True)