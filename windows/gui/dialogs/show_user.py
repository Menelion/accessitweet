# -*- coding: utf-8 -*-
import wx, twitter, config, gui.dialogs, sound, webbrowser

class showUserProfileDialog(wx.Dialog):
 def __init__(self, parent):
  self.parent = parent
  wx.Dialog.__init__(self, None, -1)
  panel = wx.Panel(self)
  userSizer = wx.BoxSizer()
  self.SetTitle(u"User Details")
  if self.parent.nb.GetCurrentPage().name_buffer == "followers" or self.parent.nb.GetCurrentPage().name_buffer == "friends":
   list = [self.parent.db.settings[self.parent.nb.GetCurrentPage().name_buffer][self.parent.nb.GetCurrentPage().get_selected()]["screen_name"]]
  else:
   list =twitter.utils.get_all_users(self.parent.db.settings[self.parent.nb.GetCurrentPage().name_buffer][self.parent.nb.GetCurrentPage().get_selected()], self.parent.db)
  self.cb = wx.ComboBox(panel, -1, choices=list, value=list[0])
  self.cb.SetFocus()
  userSizer.Add(self.cb)
  sizer = wx.BoxSizer(wx.VERTICAL)
  ok = wx.Button(panel, wx.ID_OK, "OK")
  ok.Bind(wx.EVT_BUTTON, self.onok)
  cancel = wx.Button(panel, wx.ID_CANCEL, "Cancel")
  btnsizer = wx.BoxSizer()
  btnsizer.Add(ok)
  btnsizer.Add(cancel)
  sizer.Add(userSizer)
  sizer.Add(btnsizer)
  panel.SetSizer(sizer)
  self.Bind(wx.EVT_CHAR_HOOK, self.onEscape, self.cb)

 def onEscape(self, ev):
  if ev.GetKeyCode() == wx.WXK_RETURN:
   self.onok(wx.EVT_BUTTON)
  ev.Skip()

 def onok(self, ev):
  self.EndModal(wx.ID_OK)

class showUserProfile(wx.Dialog):
 def __init__(self, twitter, screen_name):
  self.twitter = twitter
  self.screen_name = screen_name
  wx.Dialog.__init__(self, None, -1, size=(650,480))
  self.SetTitle(u"Details for %s" % (screen_name))
  panel = wx.Panel(self)
  self.get_data()
  static = wx.StaticText(panel, -1, u"Details")
  text = wx.TextCtrl(panel, -1, style=wx.TE_MULTILINE|wx.TE_READONLY, size=(500,500))
  text.SetFocus()
  self.url = wx.Button(panel, -1, u"Go to URL")
  self.url.Bind(wx.EVT_BUTTON, self.onUrl)
  self.url.Disable()
  close = wx.Button(panel, wx.ID_CANCEL, u"Cancel")
  close.Bind(wx.EVT_BUTTON, self.onClose)
#  self.Bind(wx.EVT_CLOSE, self.onClose)
  text.ChangeValue(self.compose_string())

 def onUrl(self, ev):
  webbrowser.open_new_tab(self.data["url"])

 def onClose(self, ev):
  self.Destroy()

 def get_data(self):
  try:
   self.data = self.twitter.twitter.show_user(screen_name=self.screen_name)
  except:
   wx.MessageDialog(self, u"This user doesn't exist on twitter.", "Error", wx.ICON_ERROR).ShowModal()
   return

 def compose_string(self):
  string = u""
  string = string + u"Screen Name: @%s\n" % (self.data["screen_name"])
  string = string + u"Name: %s\n" % (self.data["name"])
  if self.data["location"] != "":
   string = string+u"Location: %s\n" % (self.data["location"])
  if self.data["url"] != None:
   string = string+u"URL: %s\n" % (self.data["url"])
   self.url.Enable()
  if self.data["description"] != "":
   string = string+u"Bio: %s\n" % (self.data["description"])
  if self.data["protected"] == True: protected = u"Yes"
  else: protected = u"No"
  string = string+u"Protected: %s\n" % (protected)
  string = string+u"Followers: %s\n friends: %s\n" % (self.data["followers_count"], self.data["friends_count"])
  string = string+u"Tweets: %s\n" % (self.data["statuses_count"])
  string = string+u"Favorites: %s" % (self.data["favourites_count"])
  return string