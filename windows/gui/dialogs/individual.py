# -*- coding: utf-8 -*-
import wx, twitter, gui.dialogs, sound, config
from mysc.repeating_timer import RepeatingTimer

class individual(wx.Dialog):
 def __init__(self, parent):
  self.parent = parent
  wx.Dialog.__init__(self, None, -1)
  panel = wx.Panel(self)
  userSizer = wx.BoxSizer()
  self.SetTitle(u"Individual Timeline")
  if self.parent.nb.GetCurrentPage().name_buffer == "followers" or self.parent.nb.GetCurrentPage().name_buffer == "friends":
   list = [self.parent.db.settings[self.parent.nb.GetCurrentPage().name_buffer][self.parent.nb.GetCurrentPage().get_selected()]["screen_name"]]
  else:
   list =twitter.utils.get_all_users(self.parent.db.settings[self.parent.nb.GetCurrentPage().name_buffer][self.parent.nb.GetCurrentPage().get_selected()], self.parent.db)
  self.cb = wx.ComboBox(panel, -1, choices=list, value=list[0])
  self.cb.SetFocus()
  userSizer.Add(self.cb)
  sizer = wx.BoxSizer(wx.VERTICAL)
  ok = wx.Button(panel, wx.ID_OK, "OK")
  ok.Bind(wx.EVT_BUTTON, self.onok)
  cancel = wx.Button(panel, wx.ID_CANCEL, "Cancel")
  btnsizer = wx.BoxSizer()
  btnsizer.Add(ok)
  btnsizer.Add(cancel)
  sizer.Add(userSizer)
  sizer.Add(btnsizer)
  panel.SetSizer(sizer)
  self.Bind(wx.EVT_CHAR_HOOK, self.onEscape, self.cb)

 def onEscape(self, ev):
  if ev.GetKeyCode() == wx.WXK_RETURN:
   self.onok(wx.EVT_BUTTON)
  ev.Skip()

 def onok(self, ev):
  user = twitter.utils.if_user_exists(self.parent.twitter.twitter, self.cb.GetValue())
  if user == None:
   wx.MessageDialog(None, u"User does not exist.", u"Error", wx.ICON_ERROR).ShowModal()
   return
  if user not in config.main["individuals"]["timelines"]:
   config.main["individuals"]["timelines"].append(user)
  else:
   wx.MessageDialog(None, u"There is already a timeline open for this user. You can not open another one.", u"Existing timeline", wx.ICON_ERROR).ShowModal()
   return
  self.parent.sound.play("create_timeline.wav")
  st = gui.buffers.base.basePanel(self.parent.nb, self.parent, user, self.parent.twitter.twitter.get_user_timeline, argumento=user, sound="ready.wav", timeline=True)
  self.parent.nb.AddPage(st, _(u"Timeline for %s") % (user))
  num = st.start_streams()
  st.put_items(num)
  st.sound = "tweet_timeline.wav"
  self.parent.stream2.disconnect()
  del self.parent.stream2
  self.parent.get_tls()
  self.parent.db.settings["buffers"].append(user)
  self.Destroy()