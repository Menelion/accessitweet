# -*- coding: utf-8 -*-
import wx
import config
import gui
import logging as original_logger
from mysc import restart
log = original_logger.getLogger("configuration")

class general(wx.Panel):
 def __init__(self, parent):
  wx.Panel.__init__(self, parent)
  sizer = wx.BoxSizer(wx.VERTICAL)
  apiCallsBox = wx.BoxSizer(wx.HORIZONTAL)
  apiCallsBox.Add(wx.StaticText(self, -1, u"Number of API calls (1 api call is 200 tweets):"))
  self.apiCalls = wx.SpinCtrl(self, -1)
  self.apiCalls.SetRange(1, 10)
  self.apiCalls.SetValue(config.main["twitter"]["max_api_calls"])
  apiCallsBox.Add(self.apiCalls)
  reverse_tlBox = wx.BoxSizer(wx.HORIZONTAL)
  self.reverse_timelines = wx.CheckBox(self, -1, u"Reverse Timelines")
#_(u"Reverse Timelines"))
  self.reverse_timelines.SetValue(config.main["streams"]["reverse_timelines"])
  reverse_tlBox.Add(self.reverse_timelines)
  audioBox = wx.BoxSizer(wx.HORIZONTAL)
  self.play_audio_sound = wx.CheckBox(self, -1, u"Play Audio sound when passing over an audio tweet")
#_(u"play"))
  self.play_audio_sound.SetValue(config.main["streams"]["play_audio_sound"])
  audioBox.Add(self.play_audio_sound)
  PositionInformationBox = wx.BoxSizer(wx.HORIZONTAL)
  self.position_information = wx.CheckBox(self, -1, u"Speak position information when switching between buffers")
#_(u"play"))
  self.position_information.SetValue(config.main["streams"]["position_information"])
  PositionInformationBox.Add(self.position_information)
  apiKeyLabel = wx.StaticText(self, -1, u"SNDUp API key:")
  self.apiKey = wx.TextCtrl(self, -1)
  self.apiKey.SetValue(config.main["sndup"]["api_key"])
  apiKeyBox = wx.BoxSizer(wx.HORIZONTAL)
  apiKeyBox.Add(apiKeyLabel)
  apiKeyBox.Add(self.apiKey)
  sizer.Add(apiCallsBox)
  sizer.Add(reverse_tlBox)
  sizer.Add(audioBox)
  sizer.Add(PositionInformationBox)
  sizer.Add(apiKeyBox)

class buffers(wx.Panel):
 def __init__(self, parent):
  wx.Panel.__init__(self, parent)
  sizer = wx.BoxSizer(wx.VERTICAL)
  self.followers_value = config.main["buffers"]["followers"]
  self.friends_value = config.main["buffers"]["friends"]
  self.favs_value = config.main["buffers"]["favorites"]
  self.followers = wx.CheckBox(self, -1, u"Show Followers")
  self.followers.SetValue(config.main["buffers"]["followers"])
  self.friends = wx.CheckBox(self, -1, u"Show Friends")
  self.friends.SetValue(config.main["buffers"]["friends"])
  self.favs = wx.CheckBox(self, -1, u"Show Favorites")
  self.favs.SetValue(config.main["buffers"]["favorites"])
  sizer.Add(self.followers)
  sizer.Add(self.friends)

class configurationDialog(wx.Dialog):
 def __init__(self, parent):
  self.parent = parent
  wx.Dialog.__init__(self, None, -1)
  panel = wx.Panel(self)
  self.SetTitle(u"AccessiTweet Preferences")
  sizer = wx.BoxSizer(wx.VERTICAL)
  notebook = wx.Notebook(panel)
  self.general = general(notebook)
  notebook.AddPage(self.general, u"General")
  self.general.SetFocus()
  self.buffers = buffers(notebook)
  notebook.AddPage(self.buffers, u"Show Buffers")
  sizer.Add(notebook)
  ok_cancel_box = wx.BoxSizer(wx.HORIZONTAL)
  ok = wx.Button(panel, wx.ID_OK, u"OK")
  ok.Bind(wx.EVT_BUTTON, self.onSave)
  cancel = wx.Button(panel, wx.ID_CANCEL, u"Cancel")
  self.SetEscapeId(cancel.GetId())
  ok_cancel_box.Add(ok)
  ok_cancel_box.Add(cancel)
  sizer.Add(ok_cancel_box)
  panel.SetSizer(sizer)

 def check_followers_change(self):
  if self.buffers.followers.GetValue() != self.buffers.followers_value:
   if self.buffers.followers.GetValue() == True:
    log.debug("Creating followers list...")
    followers = gui.buffers.base.followersPanel(self.parent.nb, self.parent, "followers", self.parent.twitter.twitter.get_followers_list, argumento=self.parent.db.settings["user_name"])
    self.parent.nb.AddPage(followers, _(u"Followers"))
    num = followers.start_streams()
    followers.put_items(num)
    self.parent.db.settings["buffers"].append("followers")
   elif self.buffers.followers.GetValue() == False:
    self.parent.nb.DeletePage(self.parent.db.settings["buffers"].index("followers"))
    self.parent.db.settings.pop("followers")
    self.parent.db.settings["buffers"].remove("followers")

 def check_friends_change(self):
  if self.buffers.friends.GetValue() != self.buffers.friends_value:
   if self.buffers.friends.GetValue() == True:
    log.debug("Creating friends list...")
    friends = gui.buffers.base.followersPanel(self.parent.nb, self.parent, "friends", self.parent.twitter.twitter.get_friends_list, argumento=self.parent.db.settings["user_name"])
    self.parent.nb.AddPage(friends, _(u"friends"))
    num = friends.start_streams()
    friends.put_items(num)
    self.parent.db.settings["buffers"].append("friends")
   elif self.buffers.friends.GetValue() == False:
    self.parent.nb.DeletePage(self.parent.db.settings["buffers"].index("friends"))
    self.parent.db.settings.pop("friends")
    self.parent.db.settings["buffers"].remove("friends")

 def check_favs_change(self):
  if self.buffers.favs.GetValue() != self.buffers.favs_value:
   if self.buffers.favs.GetValue() == True:
    log.debug("Creating favorites...")
    favs = gui.buffers.base.basePanel(self.parent.nb, self.parent, "favs", self.parent.twitter.twitter.get_favorites)
    self.parent.nb.AddPage(favs, _(u"Favorites"))
    num = favs.start_streams()
    favs.put_items(num)
    self.parent.db.settings["buffers"].append("favs")
   elif self.buffers.favs.GetValue() == False:
    self.parent.nb.DeletePage(self.parent.db.settings["buffers"].index("favs"))
    self.parent.db.settings.pop("favs")
    self.parent.db.settings["buffers"].remove("favs")

 def onSave(self, ev):
  config.main["twitter"]["max_api_calls"] = self.general.apiCalls.GetValue()
  if config.main["streams"]["reverse_timelines"] != self.general.reverse_timelines.GetValue():
   config.main["streams"]["reverse_timelines"] = self.general.reverse_timelines.GetValue()
   wx.MessageDialog(None, _(u"You must restart the application. Press OK to restart it now."), _("Restart AccessiTweet"), wx.OK).ShowModal()
   restart.restart_program()
  config.main["streams"]["play_audio_sound"] = self.general.play_audio_sound.GetValue()
  config.main["streams"]["position_information"] = self.general.position_information.GetValue()
  config.main["sndup"]["api_key"] = self.general.apiKey.GetValue()
  config.main["buffers"]["followers"] = self.buffers.followers.GetValue()
  self.check_followers_change()
  config.main["buffers"]["friends"] = self.buffers.friends.GetValue()
  self.check_friends_change()
  config.main["buffers"]["favorites"] = self.buffers.favs.GetValue()
  self.check_favs_change()
  config.main.write()
  self.EndModal(wx.ID_OK)