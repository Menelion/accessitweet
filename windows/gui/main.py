# -*- coding: utf-8 -*-
import wx
import dialogs
import buffers
import config
import db
import twitter
import webbrowser
import sound
import updater
import application
import os
import logging as original_logger
import output
import platform
import urllib2
import sysTrayIcon
from mysc import event
from mysc.thread_utils import call_threaded
from twython import TwythonError
from mysc.repeating_timer import RepeatingTimer
from keyboard_handler.wx_handler import WXKeyboardHandler

log = original_logger.getLogger("gui.main")

class mainFrame(wx.Frame):
 """ Main class of the Frame. This is the Main Window."""

 ### MENU
 def makeMenus(self):
  """ Creates, bind and returns the menu bar for the application. Also in this function, the accel table is created."""
  menuBar = wx.MenuBar()

  # Application menu
  app = wx.Menu()
  updateProfile = app.Append(wx.NewId(), _(u"Update profile..."))
  self.Bind(wx.EVT_MENU, self.onUpdateProfile, updateProfile)
  show_hide = app.Append(wx.NewId(), _(u"Hide Window"))
  self.Bind(wx.EVT_MENU, self.onShow_hide, show_hide)
  prefs = app.Append(wx.NewId(), _(u"Preferences..."))
  self.Bind(wx.EVT_MENU, self.onPrefs, prefs)
  close = app.Append(wx.NewId(), _(u"Exit"))
  self.Bind(wx.EVT_MENU, self.onClose, close)

  # Tweet menu
  tweet = wx.Menu()
  compose = tweet.Append(wx.NewId(), _(u"New Tweet..."))
  self.Bind(wx.EVT_MENU, self.onCompose, compose)
  response = tweet.Append(wx.NewId(), _(u"Reply..."))
  self.Bind(wx.EVT_MENU, self.onResponse, response)
  retweet = tweet.Append(wx.NewId(), _(u"Retweet..."))
  self.Bind(wx.EVT_MENU, self.onRetweet, retweet)
  fav = tweet.Append(wx.NewId(), _(u"Favorite Tweet"))
  self.Bind(wx.EVT_MENU, self.onFav, fav)
  unfav = tweet.Append(wx.NewId(), _(u"Unfavorite Tweet"))
  self.Bind(wx.EVT_MENU, self.onUnfav, unfav)
  view = tweet.Append(wx.NewId(), _(u"View Tweet..."))
  self.Bind(wx.EVT_MENU, self.onView, view)
  delete = tweet.Append(wx.NewId(), _(u"Delete Tweet"))
  self.Bind(wx.EVT_MENU, self.onDelete, delete)

  # User menu
  user = wx.Menu()
  follow = user.Append(wx.NewId(), _(u"Follow"))
  self.Bind(wx.EVT_MENU, self.onFollow, follow)
  unfollow = user.Append(wx.NewId(), _(u"Unfollow"))
  self.Bind(wx.EVT_MENU, self.onUnfollow, unfollow)
  report = user.Append(wx.NewId(), _(u"Report as spam"))
  self.Bind(wx.EVT_MENU, self.onReport, report)
  block = user.Append(wx.NewId(), _(u"Block"))
  self.Bind(wx.EVT_MENU, self.onBlock, block)
  dm = user.Append(wx.NewId(), _(u"Send Direct message"))
  self.Bind(wx.EVT_MENU, self.onDm, dm)
  details = user.Append(wx.NewId(), _(u"Show user profile"))
  self.Bind(wx.EVT_MENU, self.onDetails, details)
  timeline = user.Append(wx.NewId(), _(u"Timeline"))
  self.Bind(wx.EVT_MENU, self.onTimeline, timeline)
  deleteTl = user.Append(wx.NewId(), _(u"Close timeline"))
  self.Bind(wx.EVT_MENU, self.onDeleteTl, deleteTl)

 # Help Menu
  help = wx.Menu()
  visit_website = help.Append(-1, _(u"AccessiTweet website"))
  self.Bind(wx.EVT_MENU, self.onVisit_website, visit_website)
  about = help.Append(-1, _(u"About AccessiTweet"))
  self.Bind(wx.EVT_MENU, self.onAbout, about)

  # Add all to the menu Bar
  menuBar.Append(app, _(u"Application"))
  menuBar.Append(tweet, _(u"Tweet"))
  menuBar.Append(user, _(u"User"))
  menuBar.Append(help, _(u"Help"))

  # Creates the acceleration table.
  self.accel_tbl = wx.AcceleratorTable([
(wx.ACCEL_CTRL, ord('T'), compose.GetId()),
(wx.ACCEL_CTRL, ord('R'), response.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('R'), retweet.GetId()),
(wx.ACCEL_CTRL, ord('F'), fav.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('F'), unfav.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('V'), view.GetId()),
(wx.ACCEL_CTRL, ord('D'), dm.GetId()),

(wx.ACCEL_CTRL, ord('Q'), close.GetId()),
(wx.ACCEL_CTRL, ord('S'), follow.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('S'), unfollow.GetId()),
(wx.ACCEL_CTRL, ord('K'), block.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('K'), report.GetId()),
(wx.ACCEL_CTRL, ord('I'), timeline.GetId()),
(wx.ACCEL_CTRL|wx.ACCEL_SHIFT, ord('I'), deleteTl.GetId()),
(wx.ACCEL_CTRL, ord('W'), show_hide.GetId()),
(wx.ACCEL_CTRL, ord('P'), updateProfile.GetId()),
  ])
  self.SetAcceleratorTable(self.accel_tbl)
  return menuBar

 ### MAIN
 def __init__(self):
  output.speak(_(u"Welcome to AccessiTweet!"))
  """ Main function of this class."""
  # Gets the database store.
  log.debug("Loading database...")
  self.pid = os.getpid()
  self.db = db.db()
  # Gets the twitter object for future calls to the twitter Rest API.
  log.debug("Getting Twitter's Rest API...")
  self.twitter = twitter.twitter.twitter()
  log.debug("Setting screen reader...")
  # Gets the sound player
  log.debug(u"Setting sound player module...")
  self.sound = sound.sounds()
  wx.Frame.__init__(self, None, -1, "AccessiTweet")
  sysTray=sysTrayIcon.SysTrayIcon(self)
  self.Maximize()
  panel = wx.Panel(self)
  self.sizer = wx.FlexGridSizer()
  # Try to connect or autorize twitter.
  self.setup_twitter()
  # Get the user name information and sync it with the db.
  log.debug("Retrieving username...")
  twitter.starting.start_user_info(self.db, self.twitter)
  self.SetTitle(u"@%s. - AccessiTweet" % (self.db.settings["user_name"]))
  self.nb = wx.Notebook(panel)
  self.Bind(wx.EVT_CLOSE, self.onClose)
  self.nb.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.onPageChanged)
  self.SetMenuBar(self.makeMenus())
  # Gets the tabs for home, mentions, send and direct messages.
  log.debug("Creating buffers...")
  self.db.settings["buffers"] = []
  home = buffers.base.basePanel(self.nb, self, "home_timeline", self.twitter.twitter.get_home_timeline)
  self.nb.AddPage(home, _(u"Home"))
  self.db.settings["buffers"].append("home_timeline")
  self.nb.GetPage(0).list.SetFocus()
  mentionsP = buffers.base.basePanel(self.nb, self, "mentions", self.twitter.twitter.get_mentions_timeline, sound="mention_received.wav")
  self.nb.AddPage(mentionsP, _("Mentions"))
  self.db.settings["buffers"].append("mentions")
  dms = buffers.base.dmPanel(self.nb, self, "direct_messages", self.twitter.twitter.get_direct_messages, sound="dm_received.wav")
  self.nb.AddPage(dms, _(u"Direct messages"))
  self.db.settings["buffers"].append("direct_messages")
  sent = buffers.base.basePanel(self.nb, self, "sent", self.twitter.twitter.get_user_timeline, argumento=self.db.settings["user_name"])
  self.nb.AddPage(sent, _(u"Sent"))
  self.db.settings["buffers"].append("sent")
# If the user has enabled favs from config.
  if config.main["buffers"]["favorites"] == True:
   log.debug("Getting Favorited tweets...")
   favs = buffers.base.basePanel(self.nb, self, "favs", self.twitter.twitter.get_favorites)
   self.nb.AddPage(favs, _(u"Favorites"))
   self.db.settings["buffers"].append("favs")
# If followers are enabled from config.
  if config.main["buffers"]["followers"] == True:
   log.debug("Getting followers...")
   followers = buffers.base.followersPanel(self.nb, self, "followers", self.twitter.twitter.get_followers_list, argumento=self.db.settings["user_name"])
   self.nb.AddPage(followers, _(u"Followers"))
   self.db.settings["buffers"].append("followers")
  # Same here but for friends.
  if config.main["buffers"]["friends"] == True:
   log.debug("Getting friends...")
   friends = buffers.base.followersPanel(self.nb, self, "friends", self.twitter.twitter.get_friends_list, argumento=self.db.settings["user_name"])
   self.nb.AddPage(friends, _(u"Friends"))
   self.db.settings["buffers"].append("friends")
  events = buffers.base.eventsPanel(self.nb)
  self.nb.AddPage(events, _(u"Events"))
  self.db.settings["buffers"].append("events")
# Setting up the individual timelines for users specified on the database.
  if config.main["individuals"].has_key("timelines") == False: config.main["individuals"]["timelines"] = []
  if len(config.main["individuals"]["timelines"]) > 0:
   log.debug("Getting individual Timelines...")
   for i in config.main["individuals"]["timelines"]:
    self.nb.AddPage(buffers.base.basePanel(self.nb, self, i, self.twitter.twitter.get_user_timeline, argumento=i), _(u"%s's timeline") % i)
    self.db.settings["buffers"].append(i)
#  if config.main["individuals"].has_key("favs") == False: config.main["individuals"]["favs"] = []
#  if len(config.main["individuals"]["favs"]) > 0:
#   log.debug("Getting individual Timelines...")
#   for i in config.main["individuals"]["favs"]:
#    self.nb.AddPage(buffers.base.basePanel(self.nb, self, i+"favs", self.twitter.twitter.get_favorites, argumento=i), _(u"Favorites for %s") % i)
#    self.db.settings["buffers"].append(i+"favs")
#   self.fav_stream = RepeatingTimer(30, self.get_non_stream_buffers)
#   self.fav_stream.start()
  self.sizer.AddMany([self.nb])
  panel.SetSizer(self.sizer)
  self.Bind(event.MyEVT_STARTED, self.onInit)
  # Start the main stream that manages home_timeline, mentions, direct_messages, favs, followers and friends.
  call_threaded(self.init, run_streams=True)
  self.showing = True

 ### Functions
 def init(self, run_streams=False):
  """ Calls the start_stream function for each stream tab."""
  for i in range(0, self.nb.GetPageCount()):
   log.info("Starting stream for %s..." % self.nb.GetPage(i).name_buffer)
   info_event = event.infoEvent(event.EVT_STARTED, 1)
   num = self.nb.GetPage(i).start_streams()
   info_event.SetItem(i, num)
   wx.PostEvent(self, info_event)
  output.speak(_(u"Ready"))
  if run_streams == True:
   self.get_home()
   self.get_tls()
   self.check_streams = RepeatingTimer(config.main["streams"]["time_to_check"], self.check_stream_up)
   self.check_streams.start()
  # If all it's done, then play a nice sound saying that all it's OK.
  self.sound.play("ready.wav")

 def get_non_stream_buffers(self):
  for i in config.main["individuals"]["favs"]:
   info_event = event.infoEvent(event.EVT_STARTED, 1)
   num = self.nb.GetPage(self.db.settings["buffers"].index(i+"favs")).start_streams()
   info_event.SetItem(self.db.settings["buffers"].index(i+"favs"), num)
   if num > 0: output.speak(_(u"%s favorites from %s") % (nun, i))
   wx.PostEvent(self, info_event)

 def setup_twitter(self):
  """ Setting up the connection for twitter, or authenticate if the config file has valid credentials."""
  if config.main['twitter']['user_key'] == '' or config.main["twitter"]["app_secret"] == '':
   log.debug("Getting Twitter Authorization...")
   url = self.twitter.get_url()
   wx.MessageDialog(self, _(u"The request for the required Twitter authorization to continue will be opened on your browser. Copy the numerical code that the social network will provide in order to login and, next, paste it in the following edit box. You only need to do it once."), _(u"Authorization"), wx.OK).ShowModal()
   webbrowser.open_new_tab(url)
   dlg = wx.TextEntryDialog(self, _(u"Enter the code here."), _(u"Verification Code"))
   if dlg.ShowModal() == wx.ID_CANCEL:
    self.Close(True)
   resp = dlg.GetValue()
   if resp == "":
    log.debug("Authorization code is empty. Exiting...")
    self.Close(True)
   else:
    self.twitter.authorize(resp)
  try:
   self.twitter.login()
   log.info("Authorized in Twitter.")
  except:
   wx.MessageDialog(self, _(u"Connection error. Try again later."), _(u"Error!"), wx.ICON_ERROR).ShowModal()
   self.Close(True)

 def get_home(self):
  """ Gets the home stream, that manages home timeline, mentions, direct messages and sent."""
  self.stream = twitter.buffers.stream.streamer(config.main["twitter"]["app_key"], config.main["twitter"]["app_secret"], config.main["twitter"]["user_key"], config.main["twitter"]["user_secret"], parent=self)
  call_threaded(self.stream.user)

 def get_tls(self):
  """ Setting the stream for individual user timelines."""
  self.stream2 = twitter.buffers.indibidual.streamer(config.main["twitter"]["app_key"], config.main["twitter"]["app_secret"], config.main["twitter"]["user_key"], config.main["twitter"]["user_secret"], parent=self)
  # The self.ids contains all IDS for the follow argument of the stream.
  ids = ""
  # If we have more than 0 items on a list, then.
  if len(config.main["individuals"]["timelines"]) > 0:
   for i in config.main["individuals"]["timelines"]:
    ids = ids+self.db.settings[i][0]["user"]["id_str"]+", "

   # Favs timelines.
#  if len(config.main["individuals"]["favs"]) > 0:
#   for i in config.main["individuals"]["favs"]:
#    id = self.db.settings[i+"favs"][0]["user"]["id_str"]
#    if id not in ids: ids = ids+id+", "
  if ids != "":
   try:
    call_threaded(self.stream2.statuses.filter, follow=ids)
   except:
    pass

 def check_stream_up(self):
  try:
   urllib2.urlopen("https://google.com", timeout=5)
  except urllib2.URLError:
   if self.stream.connected == True: self.stream.disconnect()
   if hasattr(self, "stream2") and self.stream2.connected: self.stream2.disconnect()
   if config.main["streams"]["announce_status"] == True: output.speak(_(u"Streams disconnected. AccessiTweet will try to reconnect in a minute."))
   return
  if self.stream.connected == False:
#   log.debug("Trying reconnects the stream...")
   del self.stream
   if config.main["streams"]["announce_status"] == True: output.speak(_(u"Reconnecting streams..."))
   call_threaded(self.init)
   self.get_home()
  if hasattr(self, "stream2") and self.stream2.connected == False:
   log.debug("Trying reconnects the timelines stream...")
   del self.stream2
   self.get_tls()

 ### Events
 def onInit(self, ev):
  self.nb.GetPage(ev.GetItem()[0]).put_items(ev.GetItem()[1])

 def onPrefs(self, ev=None):
  dlg = dialogs.configuration.configurationDialog(self)
  dlg.ShowModal()
  dlg.Destroy()

 def onUpdateProfile(self, ev=None):
  dialogs.update_profile.updateProfile(self).ShowModal()

 def onManual(self, ev):
  os.chdir("documentation/")
  webbrowser.open_new_tab("leeme.html")
  os.chdir("../")

 def onChangelog(self, ev):
  os.chdir("documentation/")
  webbrowser.open_new_tab("cambios.html")
  os.chdir("../")

 def onVisit_website(self, ev):
  webbrowser.open_new_tab("http://twblue.com.mx")

 def onReportBug(self, ev):
  dialogs.report_an_error.reportBug().ShowModal()

 def onCheckForUpdates(self, ev):
  updater.update_manager.check_for_update(msg=True)

 def onDetails(self, ev=None):
  """ This function shows details for the selected user."""
  if self.nb.GetCurrentPage().name_buffer == "followers" or self.nb.GetCurrentPage().name_buffer == "friends":
   list = [self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["screen_name"]]
  else:
   list =twitter.utils.get_all_users(self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()], self.db)
  dlg = dialogs.show_user.showUserProfileDialog(self)
  if dlg.ShowModal() == wx.ID_OK:
   dialogs.show_user.showUserProfile(self.twitter, dlg.cb.GetValue()).ShowModal()
   dlg.Destroy()

 def onDelete(self, ev=None):
  """ Deleting a tweet or direct message."""
  if self.nb.GetCurrentPage().name_buffer != "followers" and self.nb.GetCurrentPage() != "friends":
   dlg = wx.MessageDialog(self, _(u"Do you really want to delete this message? It will be eliminated from Twitter as well."), _(u"Delete"), wx.ICON_QUESTION|wx.YES_NO)
   if dlg.ShowModal() == wx.ID_YES:
    self.nb.GetCurrentPage().destroy_status(wx.EVT_MENU)
   else:
    return

 def onPageChanged(self, ev):
  """ Announces the new title for the tab."""
  output.speak(self.nb.GetPageText(self.nb.GetSelection())+",", True)

 def onClose(self, ev=None):
  """ Closes each stream tab clearly, saving some data."""
  log.debug("Exiting...")
  output.speak(_(u"exiting..."), True)
  try:
   self.check_streams.cancel()
  except AttributeError:
   pass
  self.sound.cleaner.cancel()
  try:
   self.stream.disconnect()
   log.debug("Stream disconnected.")
  except:
   pass
  try:
   self.stream2.disconnect()
   log.debug(u"Timelines stream disconnected.")
  except:
   pass
   log.debug(u"Deleting %s's list..." % self.nb.GetPage(i).name_buffer)
  config.main.write()
  wx.GetApp().ExitMainLoop()

 def onFollow(self, ev=None):
  """ Opens the follow dialog."""
  dialogs.follow.follow(self.nb.GetCurrentPage(), "follow").ShowModal()

 def onUnfollow(self, ev=None):
  """ Opens the unfollow dialog."""
  dialogs.follow.follow(self.nb.GetCurrentPage(), "unfollow").ShowModal()

 def onReport(self, ev=None):
  """ Opens the report dialog, to report as spam to the specified user."""
  dialogs.follow.follow(self.nb.GetCurrentPage(), "report").ShowModal()

 def onBlock(self, ev=None):
  """ Opens the "block" dialog, to block the user that you want."""
  dialogs.follow.follow(self.nb.GetCurrentPage(), "block").ShowModal()

 def onCompose(self, ev=None):
  """ Opens the new tweet dialog."""
  self.nb.GetCurrentPage().post_status(ev)

 def onResponse(self, ev=None):
  """ Opens the response dialog."""
  self.nb.GetCurrentPage().onResponse(ev)

 def onDm(self, ev=None):
  """ Opens the DM Dialog."""
  # The direct_messages buffer has a method to post a diret messages while the other tabs does has not it. 
  if self.nb.GetCurrentPage().name_buffer == "direct_messages":
   self.nb.GetCurrentPage().onResponse(ev)
  else:
#   dialogs.message.dm(_(u"Direct message to %s ") % (self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["user"]["screen_name"]), "", "", self.nb.GetCurrentPage()).ShowModal()
   self.nb.GetCurrentPage().onDm(ev)

 def onRetweet(self, ev=None):
  if self.nb.GetCurrentPage().name_buffer != "direct_messages" and self.nb.GetCurrentPage().name_buffer != "followers" and self.nb.GetCurrentPage().name_buffer != "friends":
   self.nb.GetCurrentPage().onRetweet(ev)

 def onView(self, ev=None):
  tweet = self.nb.GetCurrentPage().compose_function(self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()], self.db)[1]
  dialogs.message.tweet(_(u"Text"), _(u"Details"), tweet, self.nb.GetCurrentPage()).ShowModal()

 def onFav(self, ev=None):
  if self.nb.GetCurrentPage().name_buffer != "direct_messages" and self.nb.GetCurrentPage().name_buffer != "followers" and self.nb.GetCurrentPage().name_buffer != "friends":
   try:
    self.twitter.twitter.create_favorite(id=self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["id"])
    self.sound.play("favorite.wav")
   except TwythonError as e:
    output.speak(_(u"Error while adding to favorites."), True)
    self.sound.play("error.wav")

 def onUnfav(self, ev=None):
  if self.nb.GetCurrentPage().name_buffer != "direct_messages" and self.nb.GetCurrentPage().name_buffer != "followers" and self.nb.GetCurrentPage().name_buffer != "friends":
   try:
    self.twitter.twitter.destroy_favorite(id=self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["id"])
   except TwythonError as e:
    output.speak(_(u"Error while removing from favorites."), True)
    self.sound.play("error.wav")
#   except:
#    pass

 def onTimeline(self, ev=None):
  if len(config.main["individuals"]) == 0:
   config.main["individuals"]["timelines"] = []
  dialogs.individual.individual(self).ShowModal()

 def onAbout(self, ev=None):
  wx.MessageDialog(self, u"AccessiTweet %s. Copyright (c) 2014, MTG Studios. Based on the TW Blue application copyright 2014 Manuel Cortez." % application.version, u"About", wx.OK).ShowModal()

 def onDeleteTl(self, ev=None):
  if self.nb.GetCurrentPage().name_buffer == "home_timeline" or self.nb.GetCurrentPage().name_buffer == "mentions" or self.nb.GetCurrentPage().name_buffer == "direct_messages" or self.nb.GetCurrentPage().name_buffer == "favs" or self.nb.GetCurrentPage().name_buffer == "followers" or self.nb.GetCurrentPage().name_buffer == "friends" or self.nb.GetCurrentPage().name_buffer == "sent":
   output.speak(_(u"This buffer is not a timeline; it can't be deleted."))
   return
  dlg = wx.MessageDialog(self, _(u"Do you really want to delete this timeline?"), _(u"Attention"), style=wx.ICON_QUESTION|wx.YES_NO)
  if dlg.ShowModal() == wx.ID_YES:
   names = config.main["individuals"]["timelines"]
   user = self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["user"]["screen_name"]
   log.info(u"Deleting %s's timeline" % user)
   if user in names:
    names.remove(user)
    self.db.settings.pop(user)
    self.stream2.disconnect()
    del self.stream2
    self.nb.DeletePage(self.db.settings["buffers"].index(user))
    self.db.settings["buffers"].remove(user)
    self.sound.play("delete_timeline.wav")
#    if self.individuals.has_key(user):
#     self.individuals.pop(user)
    self.get_tls()

 ### Hidden Window
 def left(self):
  num = self.nb.GetSelection()
  if num == 0:
   self.nb.ChangeSelection(self.nb.GetPageCount()-1)
  else:
   self.nb.ChangeSelection(num-1)
  try:
   if config.main["streams"]["position_information"] == True:
    msg = _(u"%s, %s of %s") % (self.nb.GetPageText(self.nb.GetSelection()), self.nb.GetCurrentPage().get_selected()+1, self.nb.GetCurrentPage().get_count())
   else:
    msg = _(u"%s") % (self.nb.GetPageText(self.nb.GetSelection()))
  except:
   if config.main["streams"]["position_information"] == True:
    msg = _(u"%s. Empty") % (self.nb.GetPageText(self.nb.GetSelection()))
   else:
    msg = _(u"%s") % (self.nb.GetPageText(self.nb.GetSelection()))
  output.speak(msg, 1)

 def right(self):
  num = self.nb.GetSelection()
  if num+1 == self.nb.GetPageCount():
   self.nb.ChangeSelection(0)
  else:
   self.nb.ChangeSelection(num+1)
  try:
   if config.main["streams"]["position_information"] == True:
    msg = _(u"%s, %s of %s") % (self.nb.GetPageText(self.nb.GetSelection()), self.nb.GetCurrentPage().get_selected()+1, self.nb.GetCurrentPage().get_count())
   else:
    msg = _(u"%s") % (self.nb.GetPageText(self.nb.GetSelection()))
  except:
   if config.main["streams"]["position_information"] == True:
    msg = _(u"%s. Empty") % (self.nb.GetPageText(self.nb.GetSelection()))
   else:
    msg = _(u"%s") % (self.nb.GetPageText(self.nb.GetSelection()))
  output.speak(msg, 1)

 def onShow_hide(self, ev=None):
  if platform.system() == "Linux": return
  keymap = {
  "control+win+w": self.onShow_hide,
   "alt+win+down": self.go_up_conversation,
  "control+win+t": self.onCompose,
  "control+win+up": self.up,
  "control+win+down": self.down,
  "control+win+left": self.left,
  "control+win+right": self.right,
  "control+win+r": self.onResponse,
  "control+win+shift+r": self.onRetweet,
  "control+win+d": self.onDm,
  "alt+win+f": self.onFav,
  "alt+win+shift+f": self.onUnfav,
  "control+win+s": self.onFollow,
  "control+win+shift+s": self.onUnfollow,
  "control+win+shift+u": self.onDetails,
  "control+win+v": self.onView,
  "control+win+f4": self.onClose,
  "control+win+i": self.onTimeline,
  "control+win+shift+i": self.onDeleteTl,
  "control+win+shift+return": self.url,
  "control+win+return": self.audio,
  "control+win+alt+up": self.volume_up,
  "control+win+alt+down": self.volume_down,
  "control+win+home": self.go_home,
  "control+win+end": self.go_end,
  "control+win+pageup": self.go_page_up,
  "control+win+pagedown": self.go_page_down,
  "alt+win+p": self.onUpdateProfile,
  "control+win+delete": self.onDelete,
  "control+win+shift+delete": self.clear_list,
}
  self.keyboard_handler = WXKeyboardHandler(self)
  self.keyboard_handler.register_keys(keymap)
  if self.showing == True:
   self.Hide()
   output.speak(_(u"Window Hidden."))
   self.showing = False
  else:
   self.Show()
   output.speak(_(u"Window Visible."))
   self.showing = True

 def clear_list(self):
  self.nb.GetCurrentPage().interact("clear_list")

 def go_up_conversation(self):
  id = self.db.settings[self.nb.GetCurrentPage().name_buffer][self.nb.GetCurrentPage().get_selected()]["in_reply_to_status_id_str"]
  pos = twitter.utils.find_reply(id, self.db.settings["home_timeline"])
  if pos != None: self.nb.GetPage(0).select_item(pos)

 def go_home(self):
  self.nb.GetCurrentPage().select_item(0)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 def go_end(self):
  self.nb.GetCurrentPage().select_item(self.nb.GetCurrentPage().get_count()-1)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 def go_page_up(self):
  if self.nb.GetCurrentPage().get_selected <= 20:
   index = 0
  else:
   index = self.nb.GetCurrentPage().get_selected() - 20
  self.nb.GetCurrentPage().select_item(index)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 def go_page_down(self):
  if self.nb.GetCurrentPage().get_selected() >= self.nb.GetCurrentPage().get_count() - 20:
   index = self.nb.GetCurrentPage().get_count()-1
  else:
   index = self.nb.GetCurrentPage().get_selected() + 20
  self.nb.GetCurrentPage().select_item(index)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 def volume_up(self):
  self.nb.GetCurrentPage().interact("volume_up")

 def volume_down(self):
  self.nb.GetCurrentPage().interact("volume_down")

 def url(self):
  self.nb.GetCurrentPage().interact("url")

 def audio(self):
  self.nb.GetCurrentPage().interact("audio")

 def up(self):
  pos = self.nb.GetCurrentPage().get_selected()
  index = self.nb.GetCurrentPage().get_selected()-1
  try:
   self.nb.GetCurrentPage().select_item(index)
  except:
   pass
  if pos == self.nb.GetCurrentPage().get_selected():
   self.sound.play("limit.wav", False)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 def down(self):
  index = self.nb.GetCurrentPage().get_selected()+1
  pos = self.nb.GetCurrentPage().get_selected()
  try:
   self.nb.GetCurrentPage().select_item(index)
  except:
   pass
  if pos == self.nb.GetCurrentPage().get_selected():
   self.sound.play("limit.wav", False)
  try:
   output.speak(self.nb.GetCurrentPage().get_message(), 1)
  except:
   pass

 ### Close App
 def Destroy(self):
  self.sysTray.Destroy()
  super(mainFrame, self).Destroy()