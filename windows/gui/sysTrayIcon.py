﻿# -*- coding: utf-8 -*-
import wx
import application
import paths
import os

class SysTrayIcon(wx.TaskBarIcon):

	def __init__(self, frame):
		super(SysTrayIcon, self).__init__()
		self.frame=frame
		icon=wx.Icon(os.path.join(paths.app_path(), "Accessitweet.ico"), wx.BITMAP_TYPE_ICO)
		self.SetIcon(icon, application.name)
		self.menu=wx.Menu()
		item=self.menu.Append(wx.ID_ANY, _(u"Tweet"))
		self.Bind(wx.EVT_MENU, frame.onCompose, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Preferences"))
		self.Bind(wx.EVT_MENU, frame.onPrefs, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Update profile"))
		self.Bind(wx.EVT_MENU, frame.onUpdateProfile, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Show / Hide"))
		self.Bind(wx.EVT_MENU, frame.onShow_hide, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Documentation"))
		self.Bind(wx.EVT_MENU, frame.onManual, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Check for updates"))
		self.Bind(wx.EVT_MENU, frame.onCheckForUpdates, item)
		item=self.menu.Append(wx.ID_ANY, _(u"Exit"))
		self.Bind(wx.EVT_MENU, frame.onClose, item)
		self.Bind(wx.EVT_TASKBAR_RIGHT_DOWN, self.onRightClick)
		self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, self.onLeftClick)

	def onRightClick(self, evt):
		self.PopupMenu(self.menu)

	def onLeftClick(self, evt):
		if (self.frame.showing):
			self.frame.SetFocus()
		else:
			self.frame.onShow_hide()

	def Destroy(self):
		self.menu.Destroy()
		super(SysTrayIcon, self).Destroy()
