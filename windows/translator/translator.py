# -*- coding: utf-8 -*-
import re
try:
    import urllib2 as request
    from urllib import quote
except:
    from urllib import request
    from urllib.parse import quote

class Translator:
    string_pattern = r"\"(([^\"\\]|\\.)*)\""
    match_string =re.compile(
                        r"\,?\[" 
                           + string_pattern + r"\," 
                           + string_pattern + r"\," 
                           + string_pattern + r"\," 
                           + string_pattern
                        +r"\]")

    def __init__(self):
        self.from_lang = ""
        self.to_lang = ""
   
    def translate(self, source):
        json5 = self._get_json5_from_google(source)
        return self._unescape(self._get_translation_from_json5(json5))

    def _get_translation_from_json5(self, content):
        result = ""
        pos = 2
        while True:
            m = self.match_string.match(content, pos)
            if not m:
                break
            result += m.group(1)
            pos = m.end()
        return result 

    def _get_json5_from_google(self, source):
        escaped_source = quote(source, '')
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19'}
        req = request.Request(
             url="http://translate.google.com/translate_a/t?client=t&ie=UTF-8&oe=UTF-8"
                 +"&sl=%s&tl=%s&text=%s" % (self.from_lang, self.to_lang, escaped_source)
                 , headers = headers)
        r = request.urlopen(req)
        return r.read().decode('utf-8')

    def _unescape(self, text):
        return re.sub(r"\\.?", lambda x:eval('"%s"'%x.group(0)), text)

languages = {
  'af': u'African',
  'sq': u'Albanian',
  'am': u'Amárico',
  'ar': u'Arabian',
  'hy': u'Armenio',
  'az': u'Azerí',
  'eu': u'Eusquera',
  'be': u'Bielorruso',
  'bn': u'Bengalí',
  'bh': u'bhojpurí',
  'bg': u'Búlgaro',
  'my': u'Birmano',
  'ca': u'Catalán',
  'chr': u'Cherokee',
  'zh': u'Chinese',
  'zh-CN': u'Chinese Simplified',
  'zh-TW': u'Chinese traditional',
  'hr': u'Croata',
  'cs': u'Check',
  'da': u'Danish',
  'dv': u'maldivo',
  'nl': 'Holandés',
  'en': u'English',
  'eo': u'Esperanto',
  'et': u'Estonian',
  'tl': u'Tagalo',
  'fi': u'Finnish',
  'fr': u'French',
  'gl': u'Gallego',
  'ka': u'Georgiano',
  'de': u'Alemán',
  'el': u'Griego',
  'gn': u'Guaraní',
  'gu': u'Guyaratí',
  'he': u'Hebrew',
  'hi': u'Hindu',
  'hu': u'Húngaro',
  'is': u'Islandés',
  'id': u'Indonesio',
  'iu': u'Inuktitut',
  'ga': u'Irlandés',
  'it': u'Italian',
  'ja': u'Japanese',
  'kn': u'Canarés',
  'kk': u'Kazajo',
  'km': u'Camboyano',
  'ko': u'Coreano',
  'ku': u'Kurdo',
  'ky': u'kirguís',
  'lo': u'Lao',
  'lv': u'Letón',
  'lt': u'Lituano',
  'mk': u'Masidonian',
  'ms': u'Malayo',
  'ml': u'Malayalam',
  'mt': u'Maltés',
  'mr': u'Maratí',
  'mn': u'Mongol',
  'ne': u'Nepalí',
  'no': u'Noruego',
  'or': u'Oriya',
  'ps': u'Pashto',
  'fa': u'Persa',
  'pl': u'Polaco',
  'pt-PT': u'Portuguese',
  'pa': u'Panyabí',
  'ro': u'Rumano',
  'ru': u'Russian',
  'sa': u'sánscrito',
  'sr': u'Serbian',
  'sd': u'Sindhi',
  'si': u'Cingalés',
  'sk': u'Eslovaco',
  'sl': u'Esloveno',
  'es': u'Spanish',
  'sw': u'Suajili',
  'sv': u'Sueco',
  'tg': u'Tayiko',
  'ta': u'Tamil',
  #'tl': 'Tagalo',
  'te': u'Telugú',
  'th': u'Tailandés',
  'bo': u'Tibetano',
  'tr': u'Turco',
  'uk': u'Ucraniano',
  'ur': u'Urdu',
  'uz': u'Uzbeko',
  'ug': u'Uigur',
  'vi': u'Vietnamita',
  'cy': u'Galés',
  'yi': u'Yiddish'
}

def available_languages():
    l = languages.keys()
    d = languages.values()
    l.insert(0, '')
    d.insert(0, "autodetect")
    return sorted(zip(l, d))
