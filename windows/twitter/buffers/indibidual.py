# -*- coding: utf-8 -*-
from twitter import compose
from twython import TwythonStreamer
import sound
from mysc import event
import wx
import config
import output

class streamer(TwythonStreamer):
 def __init__(self, app_key, app_secret, oauth_token, oauth_token_secret, timeout=300, retry_count=None, retry_in=10, client_args=None, handlers=None, chunk_size=1, parent=None):
  self.db = parent.db
  self.parent = parent
  TwythonStreamer.__init__(self, app_key, app_secret, oauth_token, oauth_token_secret, timeout=60, retry_count=None, retry_in=180, client_args=None, handlers=None, chunk_size=1)
  self.num = 3+config.main["buffers"]["favorites"]+config.main["buffers"]["followers"]+config.main["buffers"]["friends"]

 def check_individual_favs(self, data):
  if len(config.main["individuals"]["favs"]) > 0:
   for i in config.main["individuals"]["favs"]:
    if data["source"]["screen_name"] == i:
     self.db.settings[i+"favs"].append(data["target_object"])
     wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index(i+"favs")), event.ResultEvent())
     output.speak(_(u"A favorite from %s") % (data["user"]["name"]))
     self.parent.sound.play("tweet_timeline.wav")

 def on_timeout(self):
  print "followers disconnected."
  output.speak(u"stream of followers was disconnected.")
  self.disconnect()

 def on_error(self, status_code, data):
  print status_code, data
#  pass

 def check_tls(self, data):
  if len(config.main["individuals"]["timelines"]) > 0:
   for i in config.main["individuals"]["timelines"]:
    if data["user"]["screen_name"] == i:
     if config.main["streams"]["reverse_timelines"] == False: self.db.settings[i].append(data)
     else: self.db.settings[i].insert(0, data)
#     try:
     wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index(i)), event.ResultEvent())
     output.speak("Tweet from %s" % (data["user"]["name"]))
     self.parent.sound.play("tweet_timeline.wav")
#     except:
#      pass

 def on_success(self, data):
  if "text" in data:
   self.check_tls(data)
  if data.has_key("event"):
   print data["event"], data["target"]
   if data["event"] == "favorite":
    self.check_individual_favs(data)