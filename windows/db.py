# -*- coding: utf-8 -*-
""" Handles storage from a durus database """
import config
import logging as original_logger
log = original_logger.getLogger("db")

class db(object):
 def __init__(self):
  self.settings = {}
